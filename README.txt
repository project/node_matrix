This module provides you a way to check the matrix of a page. You can check the user matrix, once you will login from admin and will view any node there will be a new tab 'matrix' will be displaying a log with 
'view','edit'.

Installation:
----------------------------------
Download module from https://www.drupal.org/project/node_matrix
Keep it in sites/all/modules/
Enable from the Admin visit node page, you will have a brand new tab, Matrix you can find the information here.